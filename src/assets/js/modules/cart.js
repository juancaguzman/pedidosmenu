app.cart = {
    sending: false,
    addtoCart: function () {
        $('.add-to-cart').on('click', function (e) {
            e.preventDefault();
            var _url = config._backend + '/food/add-to-cart';
            var _element = $(this);
            var _data = {
                _token: config._token
            };
            var _params = _element.data('role').split('-');
            var _parent = _element.parent();
            _data.id = _parent.data('id');
            _data.type = _parent.data('type');
            _data.role = _params[0];
            _data.action = _params[1];
            if (!app.cart.sending) {
                app.cart.sending = true;
                $.post(_url, _data, function (response) {
                    app.cart.sending = false;
                    if (response.success) {
                        if (_data.role === 'food' || _data.role === 'additional') {
                            $('#cart').html(response.order);
                            if(_data.role === 'additional'){
                                $('#total_price').html(response.total_price);
                            }
                            if (response.item !== null) {
                                $('#'+_data.role + _data.type + _data.id).html(response.item.quantity);
                            }
                        }
                        if (_data.role === 'saladdress' || _data.role === 'lemon') {
                            $('#' + response.role + 'Counter').html(response.counter);
                        }
                    } else {
                        notify("Lo sentimos, puedes intentarlo mas tarde?");
                    }
                });
            }
        });
    },
    confirmOrder: function () {
        var _btn = $('#confirmOrderBtn');
        if (_btn.length) {
            _btn.on('click', function (e) {
                e.preventDefault();
                if ($('#cart').find('li').length !== 0) {
                    if ($('#logged').val() !== 'false') {
                        window.location.href = '/checkout';
                    } else {
                        $('#loginModal').modal('show');
                    }
                } else {
                    notify("No te olvides de elegir tu menú");
                }
            });
        }
    },
    hasAddress: function () {
        var _check = $('#hasAddress');
        if (_check.length) {
            _check.on('change', function () {
                if (_check.prop('checked')) {
                    $('#address_name').addClass('hidden');
                    $('#address_id').removeClass('hidden');
                } else {
                    $('#address_id').addClass('hidden');
                    $('#address_name').removeClass('hidden');
                }
            });
        }
    },
    finish_order: function () {
        var _btn = $('#finish_order');
        if (_btn.length) {
            _btn.on('click', function (e) {
                e.preventDefault();
                var _url = config._backend + '/food/finish-order';
                var _data = {
                    _token: config._token,
                };
                $address_elem = $('#address_name');
                var addr_id = $address_elem.data('addrid');
                var addr_lat = $address_elem.data("lat");
                var addr_lng = $address_elem.data("lng");
                if (addr_id) {
                    _data.hasAddress = true;
                    _data.address = addr_id;
                } else {
                    _data.hasAddress = false;
                    _data.address = $('#address_name').val();
                }
                _data.lat = addr_lat;
                _data.lng = addr_lng;
                _data.save_address = $('#save_address').prop('checked');
                _data.reference = $('#reference').val();
                _data.pay_amount = $('#pay_amount').val();
                _data.cutlery = $('#cutlery').prop('checked');
                _data.terms = $('#terms').prop('checked');
                if(_data.address === ''){
                    notify('Debes colocar una dirección para continuar tu pedido');
                    return false;
                }
                if(_data.pay_amount === ''){
                    notify('Debes colocar el monto de pago para continuar tu pedido');
                    return false;
                }
                if(!_data.terms){
                    notify('Debes aceptar los términos para continuar tu pedido');
                    return false;
                }
                $.post(_url, _data, function (response) {
                    if(response.success){
                        window.location.href = '/confirmacion';
                    }else{
                        notify(response.msg);
                        return false;
                    }
                });
            });
        }
    },
    build: function () {
        app.cart.addtoCart();
        app.cart.confirmOrder();
        app.cart.hasAddress();
        app.cart.finish_order();
        console.log('###Cart###');
    }
};