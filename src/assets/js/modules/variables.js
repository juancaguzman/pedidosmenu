var notify = function(text){
    $.notify({
        icon: 'glyphicon glyphicon-warning-sign',
        message: text,
    },{
        type: 'warning',
        placement: {
            from: "top",
            align: "right"
        },
        delay: 2000
    });
};

var app = [];

var config = {
    _token: null,
    _backend: '',
};