app.login = {
    loginBtn: function () {
        var url = config._backend + '/user/login';
        var _element = $('#loginBtn');
        var _form = $('#loginForm');

        if (_element.length) {
            _element.on('click', function (e) {
                e.preventDefault();

                var _data = _form.serializeArray();

                var jsondata = {"_token": _data[0].value,
                                "phone": _data[1].value,
                                "password": _data[2].value,
                                "hasLoginFromLanding": true,
                                };

                request = $.ajax({
                    url: url,
                    type: "POST",
                    data: jsondata,
                    dataType: "json"
                });

                request.done(function (response, textStatus, jqXHR) {

                    if (response.success) {
                        if (response.actions == 'validate') {
                            $('#validatePhoneCodeModal').modal('show');
                            return false;
                        } else {
                            var _isFromMenu = $('#isFromMenu');
                            if (_isFromMenu.length) {
                                window.location.href = '/checkout';
                            } else {
                                $('#loginModal').modal('hide');
                                window.location.href = '/';
                            }
                        }
                    } else {
                        $.each(response.errors, function (i, v) {
                            _form.find('[name=' + i + ']')
                                .attr('data-content', v)
                                .popover('show');
                            return false;
                        });
                    }
                });
            });
        }
    },
    build: function () {
        app.login.loginBtn();
        console.log('###Login###');
    }
};