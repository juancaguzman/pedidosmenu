app.register = {
    registerBtn: function () {
        var url = config._backend + '/user/register';
        var _element = $('#registerBtn');
        var _form = $('#registerForm');

        if (_element.length) {
            _element.on('click', function (e) {
                e.preventDefault();
                var _data = _form.serializeArray();
                $.post(url, _data, function (response) {
                    if (response.success) {
                        $('#registerModal').modal('hide');
                        $('#validatePhoneCodeModal').modal('show');
                    } else {
                        $.each(response.errors, function (i, v) {
                            console.log(i, v);
                            _form.find('[name=' + i + ']')
                                .attr('data-content', v)
                                .popover('show');
                            return false;
                        });
                    }
                });
            });
        }
    },
    validatePhoneBtn: function () {
        var url = config._backend + '/user/validate-phone-code';
        var _element = $('#validatePhoneCodeBtn');
        var _form = $('#validatePhoneCodeForm');

        if (_element.length) {
            _element.on('click', function (e) {
                e.preventDefault();
                var _data = _form.serializeArray();

                var jsondata = {
                    "_token": _data[0].value,
                    "phone_code": _data[1].value,
                    "hasLoginFromLanding": true,
                };

                request = $.ajax({
                    url: url,
                    type: "POST",
                    data: jsondata,
                    dataType: "json"
                });

                request.done(function (response, textStatus, jqXHR) {

                    if (response.success) {
                        if (response.actions == 'validate') {
                            $('#validatePhoneCodeModal').modal('show');
                            return false;
                        } else {
                            var _isFromMenu = $('#isFromMenu');
                            if (_isFromMenu.length) {
                                window.location.href = '/checkout';
                            } else {
                                $('#loginModal').modal('hide');
                                window.location.href = '/';
                            }
                        }
                    } else {
                        $.each(response.errors, function (i, v) {
                            _form.find('[name=' + i + ']')
                                .attr('data-content', v)
                                .popover('show');
                            return false;
                        });
                    }
                });

                // $.post(url, _data, function (response) {
                //     if (response.success) {
                //         $('#validatePhoneCodeModal').modal('hide');
                //         window.location.href = '/menu';
                //     } else {
                //         $.each(response.errors, function (i, v) {
                //             console.log(i, v);
                //             _form.find('[name=' + i + ']')
                //                 .attr('data-content', v)
                //                 .popover('show');
                //             return false;
                //         });
                //     }
                // });
            });
        }
    },
    build: function () {
        app.register.registerBtn();
        app.register.validatePhoneBtn();
        console.log('###Register###');
    }
};