app.findRestaurant = {
    main: function () {
        var hasLogin = $("#hasLogin").val();
        if (hasLogin != null && hasLogin != "") {
            app.findRestaurant.openLocationContent();
        }
        var $menu_button = $('#menu-btn');
        var $close_button = $('#close-location');
        var $show_menu_button = $('#show-menu');

        $menu_button.on('click', function() {
            app.findRestaurant.openLocationContent();
        });

        $close_button.on('click', function () {
            app.findRestaurant.closeLocationContent();
        });

        // Disabled button of show menu until the user enter the position
        $show_menu_button.attr('disabled', true);

        $show_menu_button.on('click', function (e) {
            e.preventDefault();
            if (!$show_menu_button.is(":disabled")) {
                // console.log(addr);
                $.ajax({
                    type: "POST",
                    url: config._backend + "/address/add",
                    data: addr,
                    dataType: "json",
                    success: function (data) {
                        console.log("Success");
                        console.log(data);
                        window.location.href = '/menu/' + restaurants_found[0].id;
                    }
                });
            }
        });

        var $span_icon = $("#show-frec-addr").find(".glyphicon");

        $("#show-frec-addr").on("click", function (e) {
            e.preventDefault();
            if ($span_icon.attr("class").includes("down")) {
                app.findRestaurant.clearMarkers();
                app.findRestaurant.showFreccuentAddress();
            } else {
                app.findRestaurant.hideFreccuentAddress();
            }
        });
    },
    setMapOnAll: function (map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    },
    clearMarkers: function () {
        app.findRestaurant.setMapOnAll(null);
        markers = [];
    },
    showFreccuentAddress: function () {
        var $span_icon = $("#show-frec-addr").find(".glyphicon");
        addr = {};
        $('#show-menu').attr('disabled', true);
        $(".frec-addr-wrapper").slideDown();
        $(".waitari-search-wrapper").hide();
        $('#show-menu').text("VER CARTA");
        $span_icon.removeClass("glyphicon-menu-down");
        $span_icon.addClass("glyphicon-menu-up");
    },
    hideFreccuentAddress: function () {
        $("#name-street").val("");
        var $span_icon = $("#show-frec-addr").find(".glyphicon");
        $(".frec-addr-wrapper").hide();
        $(".waitari-search-wrapper").show();
        $('#show-menu').text("VER CARTA");
        $span_icon.removeClass("glyphicon-menu-up");
        $span_icon.addClass("glyphicon-menu-down");
    },
    openLocationContent: function () {
        document.getElementById("location-overlay").style.display = "block";
    },
    closeLocationContent: function () {
        document.getElementById("location-overlay").style.display = "none";
    },
    build: function () {
        app.findRestaurant.main();
        console.log('###Find a Restaurant###');
    }
}