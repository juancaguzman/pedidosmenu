app.recoveryPass = {
    recoveryPasswordBtn: function () {
        var _element = $('#recoveryPasswordBtn');
        var _form = $('#recoveryPasswordForm');
        if (_element.length) {
            _element.on('click', function (e) {
                e.preventDefault();
                var url = config._backend + '/user/recovery-pass';
                var _data = _form.serializeArray();
                $.post(url, _data, function (response) {
                    if (response.success) {
                        $('#recoveryPasswordModal').modal('hide');
                        notify(response.msg);
                    } else {
                        $.each(response.errors, function (i, v) {
                            console.log(i, v);
                            _form.find('[name=' + i + ']')
                                .attr('data-content', v)
                                .popover('show');
                            return false;
                        });
                    }
                });
            });
        }
    },
    build: function () {
        app.recoveryPass.recoveryPasswordBtn();
        console.log('###RecoveryPass###');
    }
};