$(document).ready(function () {
    $('[popover-validate]').popover({
        container: 'body',
        placement: 'top',
        trigger: 'focus',
        animation: false,
        html: true,
    });

    $('input, select, textarea, radio, checkbox').on('focus change', function () {
        $(this).popover('destroy');
    });

    config._token = $('[name="_token"]').val();

    for(item in app){
        app[item].build();
    }
});