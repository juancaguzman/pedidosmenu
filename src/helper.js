export default {
  data: {
    cart: [],
    sum: []
  },

  inc(producto) {
    var busqueda = this.data.cart.find(function(m) {
      return m.id === producto.id;
    });
    if (typeof busqueda != "object") {
      this.data.cart.push({
        id: producto.id,
        name: producto.name,
        price: producto.price,
        src: "./arroz_tapado.png",
        qty: 1
      });
    } else {
      var index = this.data.cart.indexOf(busqueda);
      this.data.cart[index].qty++;
    }
  },
  dec(producto) {
    var busqueda = this.data.cart.find(function(m) {
      return m.id === producto.id;
    });
    if (typeof busqueda == "object") {
      var index = this.data.cart.indexOf(busqueda);
      if (this.data.cart[index].qty == 1) {
        this.data.cart.splice(index, 1);
      } else {
        this.data.cart[index].qty--;
      }
    }
    // console.log(this.data.cart);
  }
};
