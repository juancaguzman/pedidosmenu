import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import Toasted from "vue-toasted";
import VTooltip from "v-tooltip";
import { service } from "./api.js";

Vue.config.productionTip = false;
Vue.use(Toasted);
Vue.use(VTooltip);
router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requireLocation)) {
    if (!store.getters.location_name) next("/");
    else next();
  } else {
    next();
  }
});
Vue.filter("currency", val => {
  return `S/. ${Number(val).toFixed(2)}`;
});


var vueapp=new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
service.getPaymeSettings().then(({ data }) => {
  store.state.paymeCredentials = data;
});

//LLAMAMOS PARA INICIALIZAR EL APLICATIVO
window.AccountKitHasInit=false;
window.AccountKit_OnInteractive = function() {
  if(!window.AccountKitHasInit){
    window.AccountKitHasInit=true;
    service.getAccountKit().then(({ data }) => {
      AccountKit.init(data);
      vueapp.$store.dispatch('finishLoad')   
    });
    setTimeout(()=>{
      document.querySelector('.root-splash').classList.add('hide');
    },2000)
  }
};

//DINAMIC LOADER
// function initPaymeScript(){
//   var head= document.getElementsByTagName('head')[0];
//   var script= document.createElement('script');
//   script.type= 'text/javascript';
//   script.id='PaymeFormScriptStarter'
//   script.src= process.env.VUE_APP_PAYME_SCRIPT;
//   head.appendChild(script);
//   console.log("gofo",head)
// }
// initPaymeScript();

//OPTIMIZACION PARA VERIFICAR CARGA DE ACCOUNT KIT
function checker(){
  var interval=setInterval(()=>{
    if(AccountKit&&AccountKit.init){
      window.AccountKit_OnInteractive();
      clearInterval(interval);
    }
  },1000)
}
checker();
