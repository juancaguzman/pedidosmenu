import Vue from "vue";
import Vuex from "vuex";
import { service } from "./api";
import VueLocalStorage from "vue-localstorage";
Vue.use(Vuex);
Vue.use(VueLocalStorage);

export default new Vuex.Store({
  state: {
    finishLoad: false,
    currentUser: {},
    currentUserId: null || Vue.localStorage.get("userId"),
    accessToken: null,
    currentLocation: null || JSON.parse(Vue.localStorage.get("location")),
    currentShop: {},
    actualZone: {},
    currentShopId: null || Vue.localStorage.get("shopId"),
    cart: JSON.parse(Vue.localStorage.get("cart"))
      ? JSON.parse(Vue.localStorage.get("cart"))
      : {
          products: []
        },
    paymeCredentials: {},
    menu: [],
    menuHours: false,
    order: JSON.parse(Vue.localStorage.get("order")) || {
      products: []
    },
    isLogged: false,
    stores: [],
    modals: {
      login: false,
      register: false,
      recovery: false,
      newpass: false
    },
    errorMessage: null
  },
  getters: {
    finishLoad: state => state.finishLoad,
    actualZone: state => state.actualZone,
    stores: state => state.stores,
    total_cost: state => {
      var total = 0;
      if (state.cart.products.length > 0) {
        for (let item of state.cart.products) {
          total += item.mainproduct.price * item.cant;
        }
      }
      return total;
    },
    location_name: state =>
      state.currentLocation ? state.currentLocation.name : undefined,
    products: state =>
      state.cart.products.map(p => {
        return {
          quantity: p.cant,
          groupId: p.groupId,
          id: p.mainproduct.id,
          price: p.mainproduct.price,
          subcat: p.sub
        };
      }),
    cantidadSelected: state => id => {
      return state.cart.products.length > 0
        ? state.cart.products.filter(p => p.mainproduct.id == id)
        : [];
    }
  },
  mutations: {
    ["SET_STORES"](state, payload) {
      state.stores = payload;
    },
    ["ADD_TO_CART"](state, payload) {
      state.cart.products.push(JSON.parse(JSON.stringify(payload)));
    },
    ["UPDATE_CART_ITEM"](state, payload) {
      state.cart.products[payload.index] = payload.datos;
      state.cart.products = new Array(...state.cart.products);
    },
    ["DELETE_FROM_CART"](state, payload) {
      state.cart.products.splice(payload.index, 1);
    },
    ["SET_LOCATION"](state, payload) {
      state.currentLocation = JSON.parse(JSON.stringify(payload));
    },
    ["SET_SHOP"](state, payload) {
      state.currentShop = JSON.parse(JSON.stringify(payload));

      if (payload) state.currentZone = payload.actualZone;
      if (payload) state.currentShopId = payload.id;
    },
    ["SET_MENU"](state, payload) {
      state.menu = new Array(...payload);
    },
    ["SET_ZONE"](state, payload) {
      state.actualZone = payload;
    },
    ["SET_USER"](state, payload) {
      state.currentUser = JSON.parse(JSON.stringify(payload));
      state.isLogged = true;
    },
    ["SET_LOGOUT"](state) {
      state.isLogged = false;
    },
    ["SET_ACCESSTOKEN"](state, payload) {
      state.accessToken = payload;
    },
    ["SET_ORDER"](state, payload) {
      state.order = JSON.parse(JSON.stringify(payload));
    },
    ["SET_HOURS"](state, payload) {
      state.menuHours = payload;
    },
    ["CLEAR_CART"](state) {
      state.cart.products.splice(0, state.cart.products.length);
    },
    ["RESET_CART"](state) {
      state.cart.products = [];
    },
    ["ATTACH_ORDER"](state, payload) {
      state.cart.order = payload;
    },
    ["SAVE_CART"](state) {
      Vue.localStorage.set("cart", JSON.stringify(state.cart));
    },
    ["CLEAN_CART"](state) {
      Vue.localStorage.remove("cart");
      state.cart = {
        products: []
      };
    },
    ["HASLOADED"](state, payload) {
      state.finishLoad = payload;
    },
    ["UPDATE_LASTORDER"](state, payload) {
      state.order = payload;
      state.order.products = JSON.parse(JSON.stringify(state.cart.products));
      Vue.localStorage.set("order", JSON.stringify(state.order));
    },
    ["OPEN_MODAL"](state, payload) {
      state.modals[payload] = true;
    },
    ["CLOSE_MODAL"](state, payload) {
      state.modals[payload] = false;
    },
    ["SET_ERRORMESSAGE"](state, payload) {
      state.errorMessage = payload;
    }
  },
  actions: {
    async getStores({ commit }) {
      var { data } = await service.getShops();
      commit("SET_STORES", data);
      return data;
    },
    finishLoad({ commit }) {
      commit("HASLOADED", true);
    },
    attachOrder({ commit }, payload) {
      commit("ATTACH_ORDER", payload);
      commit("SAVE_CART");
    },
    async confirmOrder({ commit }, payload) {
      var data = await service.confirmOrder(payload);
      commit("UPDATE_LASTORDER", data.data);
      setTimeout(() => {
        commit("CLEAN_CART");
      }, 1000);
      return data;
    },
    async signPurchase({ commit }, payload) {
      var { data } = await service.signPurchase(payload);
      return data;
    },
    toCart({ commit, state }, payload) {
      if (
        state.cart.products.some(
          i => i.mainproduct.id === payload.mainproduct.id
        )
      ) {
        commit("UPDATE_CART_ITEM", {
          index: state.cart.products.findIndex(
            i => i.mainproduct.id === payload.mainproduct.id
          ),
          datos: payload
        });
      } else {
        commit("ADD_TO_CART", payload);
      }
      commit("SAVE_CART");
    },
    fromCart({ commit, state }, payload) {
      if (
        state.cart.products.some(
          i => i.mainproduct.id === payload.mainproduct.id
        )
      ) {
        if (
          state.cart.products[
            state.cart.products.findIndex(
              i => i.mainproduct.id === payload.mainproduct.id
            )
          ].cant == 1
        ) {
          commit("DELETE_FROM_CART", {
            index: state.cart.products.findIndex(
              i => i.mainproduct.id === payload.mainproduct.id
            )
          });
        } else {
          commit("UPDATE_CART_ITEM", {
            index: state.cart.products.findIndex(
              i => i.mainproduct.id === payload.mainproduct.id
            ),
            datos: payload
          });
        }
      }
      commit("SAVE_CART");
    },
    setLocation({ commit }, payload) {
      commit("SET_LOCATION", payload);
      Vue.localStorage.set("location", JSON.stringify(payload));
    },
    async shopNearby({ commit, state }, payload) {
      var res = await service.isNearShop(payload);
      if (res.status == 200) {
        commit("SET_SHOP", res.data);
        commit("SET_ERRORMESSAGE", null);
        Vue.localStorage.set("shopId", res.data.id);
      } else {
        commit("SET_SHOP", null);
        commit("SET_ERRORMESSAGE", res.data.detail);
      }
      return res.status;
    },
    async getMenu({ commit }, payload) {
      var res = await service.getMenu(payload);
      commit("SET_MENU", res.data);
      var date = new Date();
      // console.log(date.getHours() < 11);
      if (date.getHours() < 11) {
        commit("SET_HOURS", true);
      } else {
        commit("SET_HOURS", false);
      }

      // return commit("SET_HOURS", true);
      // var date = new Date();
      // commit("SET_HOURS", true);
    },
    async getZone({ commit }, payload) {
      var res = await service.getZone(payload);
      commit("SET_ZONE", res.data);
    },
    setRegister({ commit, dispatch }, payload) {
      // console.log(payload);
      // commit("SET_USER", payload.user);
      commit("SET_ACCESSTOKEN", payload.accessToken);
      Vue.localStorage.set("accessToken", payload.accessToken);
      Vue.localStorage.set("userId", payload.user.id);
      dispatch("getUser", payload.user.id);
    },
    async postOrder({ commit }, payload) {
      var res = await service.postOrder(payload);
      if (res.status == 200) {
        commit("SET_ORDER", res.data);
        return res;
      }
      return res;
    },
    async login({ commit }, payload) {
      var res = await service.login(payload);
      if (!res.response) {
        commit("SET_USER", res.data.user);
        Vue.localStorage.set("userId", res.data.user.id);
        return res;
      }
      return res.response;
    },
    logout({ commit }) {
      commit("CLEAR_CART");
      Vue.localStorage.remove("userId");
      Vue.localStorage.remove("accessToken");
      Vue.localStorage.remove("location");
      Vue.localStorage.remove("shopId");
      Vue.localStorage.remove("cart");
      Vue.localStorage.remove("order");
      commit("SET_LOGOUT");
    },
    cleanCart({ commit, state }) {
      commit("CLEAN_CART");
      // commit("RESET_CART");
    },
    async getUser({ commit }, payload) {
      var res = await service.getUser(payload);
      commit("SET_USER", res.data);
      return res.data;
    },
    toggleModals({ commit }, payload) {
      if (payload.close) {
        commit("CLOSE_MODAL", payload.close);
      }
      if (payload.open) {
        commit("OPEN_MODAL", payload.open);
      }
    },
    async updateUser({ commit }, payload) {
      var res = await service.updateUser(payload);
      if (!res.response) {
        // commit("SET_USER", res.data.user);
        return res;
      }
      return res.response;
    }
  }
});
