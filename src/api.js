import axios from "axios";
import Vue from "vue";
import VueLocalStorage from "vue-localstorage";

Vue.use(VueLocalStorage);
let instance = null;
const VUE_APP_BASE_URL =
  process.env.VUE_APP_BASE_URL || "http://api.waitari.com/api/v1/";

const HTTP = axios.create({
  baseURL: VUE_APP_BASE_URL,
  headers: {
    "X-Access-Token": `${Vue.localStorage.get("accessToken")}`
  }
});

class Service {
  constructor() {
    if (!instance) {
      instance = this;
    }
    return instance;
  }
  /**
   * 
   * @REGISTER 
   */
  getAccountKit() {
    return HTTP.get(`/account-kit`);
  }
  AccountKitExchange(payload) {
    return HTTP.post(`/account-kit/exchange`, payload)
      .then(res => res)
      .catch(err => err);
  }
  AccountKitRecovery(payload) {
    return HTTP.post(`/account-kit/recovey`, payload)
      .then(res => res)
      .catch(err => err.response);
  }
  // async loginCallback(response) {
  //   if (response.status === "PARTIALLY_AUTHENTICATED") {
  //     var code = response.code;
  //     var csrf = response.state;
  //     var res = AccountKitExchange({
  //       code: code,
  //       state: csrf
  //     });
  //     console.log(res.data);
  //     return res;
  //     // Send code to server to exchange for access token
  //   } else if (response.status === "NOT_AUTHENTICATED") {
  //     // handle authentication failure
  //   } else if (response.status === "BAD_PARAMS") {
  //     // handle bad parameters
  //   }
  // }
  login(payload) {
    return HTTP.post(`/users/login/`, payload)
      .then(res => res)
      .catch(err => err);
  }
  getPaymeSettings() {
    return HTTP.get(`/pay-me`);
  }
  signPurchase(order) {
    return HTTP.post(`/pay-me/sign`, order);
  }
  updateUser(payload) {
    return HTTP.put(`users/${payload.id}`, payload.data);
  }
  getUser(payload) {
    return HTTP.get(`/users/${payload}/`);
  }
  getShops() {
    return HTTP.get(`/shops`);
  }
  isNearShop(payload) {
    return HTTP.get(`/findbylocation?lat=${payload.lat}&long=${payload.long}`)
      .then(res => res)
      .catch(err => err.response);
  }
  getMenu(payload) {
    var d = new Date();
    return HTTP.get(`/shops/${payload}/products?date=${d.toISOString()}`);
  }
  getZone(payload) {
    return HTTP.get(`/zones/${payload}`);
  }
  postOrder(payload) {
    return HTTP.post(`/shops/${payload.shopId}/orders`, payload)
      .then(res => res)
      .catch(err => err.response);
  }
  confirmOrder(payload) {
    return HTTP.put(`/shops/${payload.shopId}/orders/confirm`, payload);
  }
}

export const service = new Service();
