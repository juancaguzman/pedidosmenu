var notify = function(text){
    $.notify({
        icon: 'glyphicon glyphicon-warning-sign',
        message: text,
    },{
        type: 'warning',
        placement: {
            from: "top",
            align: "right"
        },
        delay: 2000
    });
};

var app = [];

var config = {
    _token: null,
    _backend: '',
};
app.login = {
    loginBtn: function () {
        var url = config._backend + '/user/login';
        var _element = $('#loginBtn');
        var _form = $('#loginForm');

        if (_element.length) {
            _element.on('click', function (e) {
                e.preventDefault();

                var _data = _form.serializeArray();

                var jsondata = {"_token": _data[0].value,
                                "phone": _data[1].value,
                                "password": _data[2].value,
                                "hasLoginFromLanding": true,
                                };

                request = $.ajax({
                    url: url,
                    type: "POST",
                    data: jsondata,
                    dataType: "json"
                });

                request.done(function (response, textStatus, jqXHR) {

                    if (response.success) {
                        if (response.actions == 'validate') {
                            $('#validatePhoneCodeModal').modal('show');
                            return false;
                        } else {
                            var _isFromMenu = $('#isFromMenu');
                            if (_isFromMenu.length) {
                                window.location.href = '/checkout';
                            } else {
                                $('#loginModal').modal('hide');
                                window.location.href = '/';
                            }
                        }
                    } else {
                        $.each(response.errors, function (i, v) {
                            _form.find('[name=' + i + ']')
                                .attr('data-content', v)
                                .popover('show');
                            return false;
                        });
                    }
                });
            });
        }
    },
    build: function () {
        app.login.loginBtn();
        console.log('###Login###');
    }
};
app.register = {
    registerBtn: function () {
        var url = config._backend + '/user/register';
        var _element = $('#registerBtn');
        var _form = $('#registerForm');

        if (_element.length) {
            _element.on('click', function (e) {
                e.preventDefault();
                var _data = _form.serializeArray();
                $.post(url, _data, function (response) {
                    if (response.success) {
                        $('#registerModal').modal('hide');
                        $('#validatePhoneCodeModal').modal('show');
                    } else {
                        $.each(response.errors, function (i, v) {
                            console.log(i, v);
                            _form.find('[name=' + i + ']')
                                .attr('data-content', v)
                                .popover('show');
                            return false;
                        });
                    }
                });
            });
        }
    },
    validatePhoneBtn: function () {
        var url = config._backend + '/user/validate-phone-code';
        var _element = $('#validatePhoneCodeBtn');
        var _form = $('#validatePhoneCodeForm');

        if (_element.length) {
            _element.on('click', function (e) {
                e.preventDefault();
                var _data = _form.serializeArray();

                var jsondata = {
                    "_token": _data[0].value,
                    "phone_code": _data[1].value,
                    "hasLoginFromLanding": true,
                };

                request = $.ajax({
                    url: url,
                    type: "POST",
                    data: jsondata,
                    dataType: "json"
                });

                request.done(function (response, textStatus, jqXHR) {

                    if (response.success) {
                        if (response.actions == 'validate') {
                            $('#validatePhoneCodeModal').modal('show');
                            return false;
                        } else {
                            var _isFromMenu = $('#isFromMenu');
                            if (_isFromMenu.length) {
                                window.location.href = '/checkout';
                            } else {
                                $('#loginModal').modal('hide');
                                window.location.href = '/';
                            }
                        }
                    } else {
                        $.each(response.errors, function (i, v) {
                            _form.find('[name=' + i + ']')
                                .attr('data-content', v)
                                .popover('show');
                            return false;
                        });
                    }
                });

                // $.post(url, _data, function (response) {
                //     if (response.success) {
                //         $('#validatePhoneCodeModal').modal('hide');
                //         window.location.href = '/menu';
                //     } else {
                //         $.each(response.errors, function (i, v) {
                //             console.log(i, v);
                //             _form.find('[name=' + i + ']')
                //                 .attr('data-content', v)
                //                 .popover('show');
                //             return false;
                //         });
                //     }
                // });
            });
        }
    },
    build: function () {
        app.register.registerBtn();
        app.register.validatePhoneBtn();
        console.log('###Register###');
    }
};
app.recoveryPass = {
    recoveryPasswordBtn: function () {
        var _element = $('#recoveryPasswordBtn');
        var _form = $('#recoveryPasswordForm');
        if (_element.length) {
            _element.on('click', function (e) {
                e.preventDefault();
                var url = config._backend + '/user/recovery-pass';
                var _data = _form.serializeArray();
                $.post(url, _data, function (response) {
                    if (response.success) {
                        $('#recoveryPasswordModal').modal('hide');
                        notify(response.msg);
                    } else {
                        $.each(response.errors, function (i, v) {
                            console.log(i, v);
                            _form.find('[name=' + i + ']')
                                .attr('data-content', v)
                                .popover('show');
                            return false;
                        });
                    }
                });
            });
        }
    },
    build: function () {
        app.recoveryPass.recoveryPasswordBtn();
        console.log('###RecoveryPass###');
    }
};
app.cart = {
    sending: false,
    addtoCart: function () {
        $('.add-to-cart').on('click', function (e) {
            e.preventDefault();
            var _url = config._backend + '/food/add-to-cart';
            var _element = $(this);
            var _data = {
                _token: config._token
            };
            var _params = _element.data('role').split('-');
            var _parent = _element.parent();
            _data.id = _parent.data('id');
            _data.type = _parent.data('type');
            _data.role = _params[0];
            _data.action = _params[1];
            if (!app.cart.sending) {
                app.cart.sending = true;
                $.post(_url, _data, function (response) {
                    app.cart.sending = false;
                    if (response.success) {
                        if (_data.role === 'food' || _data.role === 'additional') {
                            $('#cart').html(response.order);
                            if(_data.role === 'additional'){
                                $('#total_price').html(response.total_price);
                            }
                            if (response.item !== null) {
                                $('#'+_data.role + _data.type + _data.id).html(response.item.quantity);
                            }
                        }
                        if (_data.role === 'saladdress' || _data.role === 'lemon') {
                            $('#' + response.role + 'Counter').html(response.counter);
                        }
                    } else {
                        notify("Lo sentimos, puedes intentarlo mas tarde?");
                    }
                });
            }
        });
    },
    confirmOrder: function () {
        var _btn = $('#confirmOrderBtn');
        if (_btn.length) {
            _btn.on('click', function (e) {
                e.preventDefault();
                if ($('#cart').find('li').length !== 0) {
                    if ($('#logged').val() !== 'false') {
                        window.location.href = '/checkout';
                    } else {
                        $('#loginModal').modal('show');
                    }
                } else {
                    notify("No te olvides de elegir tu menú");
                }
            });
        }
    },
    hasAddress: function () {
        var _check = $('#hasAddress');
        if (_check.length) {
            _check.on('change', function () {
                if (_check.prop('checked')) {
                    $('#address_name').addClass('hidden');
                    $('#address_id').removeClass('hidden');
                } else {
                    $('#address_id').addClass('hidden');
                    $('#address_name').removeClass('hidden');
                }
            });
        }
    },
    finish_order: function () {
        var _btn = $('#finish_order');
        if (_btn.length) {
            _btn.on('click', function (e) {
                e.preventDefault();
                var _url = config._backend + '/food/finish-order';
                var _data = {
                    _token: config._token,
                };
                $address_elem = $('#address_name');
                var addr_id = $address_elem.data('addrid');
                var addr_lat = $address_elem.data("lat");
                var addr_lng = $address_elem.data("lng");
                if (addr_id) {
                    _data.hasAddress = true;
                    _data.address = addr_id;
                } else {
                    _data.hasAddress = false;
                    _data.address = $('#address_name').val();
                }
                _data.lat = addr_lat;
                _data.lng = addr_lng;
                _data.save_address = $('#save_address').prop('checked');
                _data.reference = $('#reference').val();
                _data.pay_amount = $('#pay_amount').val();
                _data.cutlery = $('#cutlery').prop('checked');
                _data.terms = $('#terms').prop('checked');
                if(_data.address === ''){
                    notify('Debes colocar una dirección para continuar tu pedido');
                    return false;
                }
                if(_data.pay_amount === ''){
                    notify('Debes colocar el monto de pago para continuar tu pedido');
                    return false;
                }
                if(!_data.terms){
                    notify('Debes aceptar los términos para continuar tu pedido');
                    return false;
                }
                $.post(_url, _data, function (response) {
                    if(response.success){
                        window.location.href = '/confirmacion';
                    }else{
                        notify(response.msg);
                        return false;
                    }
                });
            });
        }
    },
    build: function () {
        app.cart.addtoCart();
        app.cart.confirmOrder();
        app.cart.hasAddress();
        app.cart.finish_order();
        console.log('###Cart###');
    }
};
app.findRestaurant = {
    main: function () {
        var hasLogin = $("#hasLogin").val();
        if (hasLogin != null && hasLogin != "") {
            app.findRestaurant.openLocationContent();
        }
        var $menu_button = $('#menu-btn');
        var $close_button = $('#close-location');
        var $show_menu_button = $('#show-menu');

        $menu_button.on('click', function() {
            app.findRestaurant.openLocationContent();
        });

        $close_button.on('click', function () {
            app.findRestaurant.closeLocationContent();
        });

        // Disabled button of show menu until the user enter the position
        $show_menu_button.attr('disabled', true);

        $show_menu_button.on('click', function (e) {
            e.preventDefault();
            if (!$show_menu_button.is(":disabled")) {
                // console.log(addr);
                $.ajax({
                    type: "POST",
                    url: config._backend + "/address/add",
                    data: addr,
                    dataType: "json",
                    success: function (data) {
                        console.log("Success");
                        console.log(data);
                        window.location.href = '/menu/' + restaurants_found[0].id;
                    }
                });
            }
        });

        var $span_icon = $("#show-frec-addr").find(".glyphicon");

        $("#show-frec-addr").on("click", function (e) {
            e.preventDefault();
            if ($span_icon.attr("class").includes("down")) {
                app.findRestaurant.clearMarkers();
                app.findRestaurant.showFreccuentAddress();
            } else {
                app.findRestaurant.hideFreccuentAddress();
            }
        });
    },
    setMapOnAll: function (map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    },
    clearMarkers: function () {
        app.findRestaurant.setMapOnAll(null);
        markers = [];
    },
    showFreccuentAddress: function () {
        var $span_icon = $("#show-frec-addr").find(".glyphicon");
        addr = {};
        $('#show-menu').attr('disabled', true);
        $(".frec-addr-wrapper").slideDown();
        $(".waitari-search-wrapper").hide();
        $('#show-menu').text("VER CARTA");
        $span_icon.removeClass("glyphicon-menu-down");
        $span_icon.addClass("glyphicon-menu-up");
    },
    hideFreccuentAddress: function () {
        $("#name-street").val("");
        var $span_icon = $("#show-frec-addr").find(".glyphicon");
        $(".frec-addr-wrapper").hide();
        $(".waitari-search-wrapper").show();
        $('#show-menu').text("VER CARTA");
        $span_icon.removeClass("glyphicon-menu-up");
        $span_icon.addClass("glyphicon-menu-down");
    },
    openLocationContent: function () {
        document.getElementById("location-overlay").style.display = "block";
    },
    closeLocationContent: function () {
        document.getElementById("location-overlay").style.display = "none";
    },
    build: function () {
        app.findRestaurant.main();
        console.log('###Find a Restaurant###');
    }
}
$(document).ready(function () {
    $('[popover-validate]').popover({
        container: 'body',
        placement: 'top',
        trigger: 'focus',
        animation: false,
        html: true,
    });

    $('input, select, textarea, radio, checkbox').on('focus change', function () {
        $(this).popover('destroy');
    });

    config._token = $('[name="_token"]').val();

    for(item in app){
        app[item].build();
    }
});